import scalaj.http._
import yamahaClient._
import scala.util.{Failure, Success}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.concurrent.duration._
import org.json4s._
import org.json4s.jackson.JsonMethods._


object Controller {
  def main(args: Array[String]): Unit = {

      val yc = yamahaClient("192.168.1.51","YamahaExtendedControl/v1")

      val result = for {
        _ <- yc.power(parameterType.powerState.on)
        _ <- yc.setInputSource(parameterType.inputSource.server)
        _ <- yc.getListInfo
        _ <- yc.setListControl(5)
        lib <- yc.getListInfo
      } yield lib

      result onComplete {
        case Success(x) => println(pretty(x))
        case Failure(x) => x.printStackTrace()
      }


    Thread.sleep(2000)

  }
}
