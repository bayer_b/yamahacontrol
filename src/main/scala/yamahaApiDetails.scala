object yamahaApiDetails {
  def responseCodeToDescription(code: Int): String = code match {
    case 0 => "Successful request"
    case 1 => "Initializing"
    case 2 => "Internal Error" /* A method did not exist, a method wasn’t appropriate etc. */
    case 3 => "Invalid Request" /* Out of range, invalid characters etc. */
    case 4 => "Invalid Parameter" /* Out of range, invalid characters etc. */
    case 5 => "Guarded" /* Unable to setup in current status etc. */
    case 6 => "Time Out"
    case 99 => "Firmware Updating"
    case 100 => "Access Error"
    case 101 => "Other Errors"
    case 102 => "Wrong User Name"
    case 103 => "Wrong Password"
    case 104 => "Account Expired"
    case 105 => "Account Disconnected/Gone Off/Shut Down"
    case 106 => "Account Number Reached to the Limit"
    case 107 => "Server Maintenance"
    case 108 => "Invalid Account"
    case 109 => "License Error"
    case 110 => "Read Only Mode"
    case 111 => "Max Stations"
    case 112 => "Access Denied"
    case _ => "Undefined"
  }

}
