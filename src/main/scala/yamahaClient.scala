import org.json4s
import org.json4s.jackson.JsonMethods.parse

import scalaj.http._
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import org.json4s._
import org.json4s.jackson.Serialization
import org.json4s.jackson.JsonMethods._

import yamahaApiDetails._


object parameterType {

  object powerState extends Enumeration {
    type powerState = Value
    val on: parameterType.powerState.Value = Value("on")
    val standBy: parameterType.powerState.Value = Value("standby")
  }

  object inputSource extends Enumeration {
    type inputSource = Value
    val server: parameterType.inputSource.Value = Value("server")
  }

}

object yamahaClientException {

  sealed trait yamahaClientException {
    self: Throwable =>
    val message: String
    val jsonResponse: String
  }

  case class responseException(customMessage: String, params: Map[String, String], responseBody: json4s.JValue) extends Exception(customMessage) with yamahaClientException {
    override val jsonResponse: String = pretty(responseBody)
    val paramString: String = params.map(_.productIterator.mkString(":")).mkString("\n\t")
    override val message: String = s"$customMessage.\nrequest parameters:\n\t$paramString\nJsonResponse:\n$jsonResponse"
    override def getMessage: String = message
  }

}

case class Info(text: String, subtexts: List[String], thumbnail: String, attributeField: Int)

case class ListData(responseCode: Int, input: String, menuLayer: Int, maxLine: Int, index: Int, playingIndex: Int,
                    menuName: String, listInfos: List[Info])


case class yamahaClient(ip: String, baseUrl: String) {

  val windowSize = 8

  implicit val formats: Formats = Serialization.formats(ShortTypeHints(List(classOf[ListData])))

  def getRequestUrl(restUrl: String): String = {
    s"http://$ip/$baseUrl/$restUrl"
  }

  def getResponse(restUrl: String,
                  urlParams: Map[String, String] = Map("" -> ""),
                  connectTimeout: Int = 5000,
                  readTimeout: Int = 5000,
                  requestMethod: String = "GET"): Future[org.json4s.JsonAST.JValue] = Future {

    val url = getRequestUrl(restUrl)
    val resp: HttpResponse[String] = Http(url).params(urlParams).timeout(connectTimeout, readTimeout).asString
    val responseBody = parse(resp.body)

    val responseCode = (responseBody \ "response_code")
      .toOption
      .getOrElse(
        throw yamahaClientException.responseException("Cannot get response code out of Json", urlParams, responseBody)
      )
      .extract[Int]

    if (responseCode == 0)
      responseBody
    else {
      val respCodeDesc = responseCodeToDescription(responseCode)
      throw yamahaClientException.responseException(s"$respCodeDesc", urlParams, responseBody)
    }
  }


  def power(state: parameterType.powerState.powerState) = Future {
    val resp: Future[org.json4s.JsonAST.JValue] = for {
      j <- getResponse("main/setPower", Map("power" -> state.toString))
    } yield j
  }

  def setInputSource(source: parameterType.inputSource.inputSource) = Future {
    val resp: Future[org.json4s.JsonAST.JValue] = for {
      j <- getResponse("main/setInput", Map("input" -> source.toString))
    } yield j
  }


  def getListInfo: Future[org.json4s.JsonAST.JValue] = {

    for {

      response <- getResponse(
        "netusb/getListInfo",
        Map("input" -> "server",
          "index" -> "0",
          "size" -> windowSize.toString,
          "lang" -> "en"
        )
      )

      maxLine = (response \ "max_line")
        .toOption
        .getOrElse(
          throw yamahaClientException.responseException("Cannot get maxLine out of Json.",
            Map("input" -> "server",
              "index" -> "0",
              "size" -> windowSize.toString,
              "lang" -> "en"
            )
            , response)
        )
        .extract[Int]

      res <- Future.sequence((0 until (maxLine/windowSize + 1)) map (i =>
        getResponse(
          "netusb/getListInfo",
          Map(
            "input" -> "server",
            "index" -> (i * windowSize).toString,
            "size" -> windowSize.min(maxLine - i * windowSize).toString,
            "lang" -> "en"
          )
        )
        )
      )

      resA = res.reduce(_ merge _)

    } yield resA

  }

  def setListControl(index: Int): Future[org.json4s.JsonAST.JValue] = {
    getResponse(
      "netusb/setListControl",
      Map(
        "list_id" -> "main",
        "type" -> "select",
        "index" -> index.toString
      )
    )
  }

}
